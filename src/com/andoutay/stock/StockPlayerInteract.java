package com.andoutay.stock;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.Acrobot.ChestShop.Utils.uSign;
import com.Acrobot.ChestShop.Items.Items;
import com.Acrobot.ChestShop.Containers.Container;
import com.Acrobot.ChestShop.Containers.MinecraftChest;
//import com.Acrobot.ChestShop.Chests.MinecraftChest;

public class StockPlayerInteract implements Listener
{
	
	@EventHandler
	public static void onPlayerInteract(PlayerInteractEvent event)
	{
		Action action = event.getAction();
		Block block = event.getClickedBlock();
		
		if (action != Action.RIGHT_CLICK_BLOCK) return;					//exiting handler due to it not being the right kind of block interact
		
		if ( !(block.getState() instanceof Sign) ) return;				//exiting due to the block not being a sign
		
		Block blockbelow = block.getRelative(0, -1, 0);
		if ( !(blockbelow.getState() instanceof Chest) ) return;		//exiting due to the block below the sign not being a chest
		
		Sign sign = (Sign)block.getState();								//exiting due to the sign being improperly formatted
		if (!uSign.isValid(sign)) return;
		
		
		Chest chest = (Chest) blockbelow.getState();
		Player player = event.getPlayer();
		
		//Inventory chestInv = chest.getInventory();
		int amt;
		String owner, amount, signMat, world = block.getWorld().getName();
		Location loc = sign.getLocation();
		boolean chestHasEnoughMat = false;
		
		//try catch around following three lines removed because there should be no problem getting data from the sign since it will be valid (handler exits if it's not - see above)
		signMat = sign.getLine(3);
		owner = sign.getLine(0);
		amount = sign.getLine(1);
		
		amt = Integer.parseInt(amount);
		if (!player.getDisplayName().equalsIgnoreCase(owner))	//owners don't buy from chests when they right click - add any other conditions here that will prevent error below:
			amt *= 2;		//recognizes almost empty chests when players are buying. If player unsuccessfully buys, will warn players that their chests are almost out of stock even though they will have one full transaction that can be made
		
		ItemStack stack = Items.getItemStack(signMat);
		short damageVal = stack.getDurability();
		Container chObj = new MinecraftChest(chest);
		chestHasEnoughMat = chObj.hasEnough(stack, amt, damageVal);
		
		//chest doesn't contain (enough of) the right material
		if (!chestHasEnoughMat)
			YmlManager.addShop(owner, loc.getX(), loc.getY(), loc.getZ(), world);
		else
			YmlManager.delShop(owner, loc.getX(), loc.getY(), loc.getZ(), world);
	}
}
