package com.andoutay.stock;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class StockPlayerJoin implements Listener
{
	@EventHandler
	public static void onPlayerJoin(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();
		if (Stock.canUseCommand(player, "Stock.check")) Stock.displayInfo(player); 
	}
}