package com.andoutay.stock;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class YmlManager {
	
	private static FileConfiguration shop = null;
	private static File shopFile = null;
	
	//I think this class should also have a removeShop function once we get what's here working
	public static ArrayList<ArrayList<String>> getPlayerShops(String playerName){
		ArrayList<ArrayList<String>> returnarray = new ArrayList<ArrayList<String>>();
		List<String> stringshops = getShop().getStringList(playerName);
		for(int i = 0; i < stringshops.size(); i++){
			String[] cords = stringshops.get(i).split(",");
			ArrayList<String> temp = new ArrayList<String>();
			for(int j = 0; j < cords.length; j++)
				temp.add(cords[j]);
			returnarray.add(temp);
		}
		return returnarray;
	}
	
	public static void addShop(String playerName, double x, double y, double z, String world)
	{
		String locString = coordsToString(x, y, z);
		
		locString += "," + world;
		
		List<String> current = getShop().getStringList(playerName);
		if (!current.contains(locString))
			current.add(locString);
		String[] knew = current.toArray(new String[current.size()]);
		getShop().set(playerName, Arrays.asList(knew));
		
		saveShop();
	}
	
	public static void delShop(String playerName, double x, double y, double z, String world)
	{
		int i = -1;
		String locString = coordsToString(x, y, z);
		
		locString += "," + world;
		
		List<String> current = getShop().getStringList(playerName);
		i = current.indexOf(locString);
		if (i >= 0)
			current.remove(i);
		String[] knew = current.toArray(new String[current.size()]);
		getShop().set(playerName, Arrays.asList(knew));
		
		saveShop();
	}
	
	public static String coordsToString(double x, double y, double z)
	{
		int temp;
		String string = "";
		temp = (int) x;
		string = temp + ",";
		temp = (int) y;
		string += temp + ",";
		temp = (int) z;
		string += temp;
		
		return string;
	}
	
	public static void reloadShop()
	{
		if (shopFile == null)
			shopFile = new File(Stock.folder, "shops.yml");
		shop = YamlConfiguration.loadConfiguration(shopFile);
	}
	
	public static FileConfiguration getShop()
	{
		if (shop == null)
			reloadShop();
		
		return shop;
	}

	public static void saveShop()
	{
		if (shop == null || shopFile == null)
		{
			return;
		}
		try
		{
			shop.save(shopFile);
		}
		catch (IOException ex)
		{
			Stock.log.severe("Could not save config to " + shopFile + ex);
		}
	}
}
