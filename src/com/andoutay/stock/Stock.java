package com.andoutay.stock;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.Acrobot.ChestShop.Utils.uInventory;
import com.Acrobot.ChestShop.Utils.uSign;
import com.Acrobot.ChestShop.Items.Items;

//todo:
//add support for buy only chests
//add support for telling a user if their chest is either almost full or almost empty (and completely empty but not completely full)

public class Stock extends JavaPlugin
{
	public static Logger log = Logger.getLogger("Minecraft");
	public static String chatPrefix = ChatColor.RESET + "[" + ChatColor.GREEN + "Stock" + ChatColor.RESET + "] ";
	public String logPrefix = "[Stock] ";
	public static File folder;

	@Override
	public void onEnable()
	{
		PluginDescriptionFile pdf = this.getDescription();

		//check that ChestShop is installed - if it isn't, 1. there's no point to this plugin, and 2. it will break - this function disables the plugin if ChestShop isn't found
		checkDependenciesInstalled(pdf);

		PluginManager pm = this.getServer().getPluginManager();

		pm.registerEvents(new StockPlayerInteract(), this);
		pm.registerEvents(new StockPlayerJoin(), this);

		folder = getDataFolder();

		log.info(logPrefix + "enabled successfully");

	}

	@Override
	public void onDisable()
	{
		getServer().getScheduler().cancelTasks(this);
		log.info(logPrefix + "disabled");
	}

	public boolean onCommand(CommandSender sender, Command cmd, String cmdLabel, String args[])
	{
		Player player = null;
		if (sender instanceof Player)
			player = (Player)sender;

		if (player != null && canUseCommand(player, "Stock.check") && cmd.getName().equalsIgnoreCase("stock") && args.length == 0)
		{
			displayInfo(player);

			return true;
		}
		else if (player != null && !canUseCommand(player, "Stock.check") && cmd.getName().equalsIgnoreCase("stock"))
		{
			player.sendMessage(chatPrefix + ChatColor.YELLOW + "You don't have permissions to use " + ChatColor.RED + "/stock");
		}
		else if (!(sender instanceof Player) && cmd.getName().equalsIgnoreCase("stock"))
		{
			sender.sendMessage(chatPrefix + "The console does not have a shop and therefore cannot have any shops in or out of stock");
			return true;
		}
		else
			return false;

		return false;	
	}

	public void checkDependenciesInstalled(PluginDescriptionFile pdf)
	{
		Plugin chestShop = getServer().getPluginManager().getPlugin("ChestShop");
		
		if (chestShop == null)	//if chestShop is not installed
		{
			log.info(logPrefix + "ChestShop not found. Disabling Stock");
			getServer().getPluginManager().disablePlugin(this);
		}
	}


	public static boolean canUseCommand(Player player, String permission)
	{
		return player.hasPermission(permission);
	}
	
	public static void displayInfo(Player player)
	{
		int i, x, y, z, numShops, numSell, amt;
		short damageVal;
		boolean remove = true;
		String worldString;
		ArrayList<ArrayList<String>> shopLocs;
		ArrayList<String> shopLoc;
		ArrayList<String> msg = new ArrayList<String>();	//will this crash if more than 10? - I don't think so
		Location loc = null;
		Block block;
		Sign sign;
		Chest chest;
		Inventory inv;
		ItemStack stack;
		World world;

		shopLocs = YmlManager.getPlayerShops(player.getDisplayName());	//Should this return a special value if it can't find the player so a different message could be displayed to the player?
		for (i = 0; i < shopLocs.size(); i++)
		{
			shopLoc = shopLocs.get(i);
			remove = true;
			x = Integer.parseInt(shopLoc.get(0));
			y = Integer.parseInt(shopLoc.get(1));
			z = Integer.parseInt(shopLoc.get(2));
			worldString = shopLoc.get(3);

			world = Bukkit.getWorld(worldString);
			loc = new Location(world, x, y, z);
			block = loc.getBlock();

			if (block.getState() instanceof Sign)
			{
				sign = (Sign) block.getState();
				block = block.getRelative(0, -1, 0);
				if (uSign.isValid(sign) && block.getState() instanceof Chest && sign.getLine(0).equalsIgnoreCase(player.getDisplayName()))
				{
					stack = Items.getItemStack(sign.getLine(3));
					damageVal = stack.getDurability();
					numSell = Integer.parseInt(sign.getLine(1));
					chest = (Chest) block.getState();
					inv = chest.getInventory();
					
					amt = uInventory.amount(inv, stack, damageVal);
					
					//log.info("amt: " + amt + " numSell " + numSell);
					
					if (amt >= numSell)
						remove = true;
					else if (amt > 0)
					{
						remove = false;
						msg.add(ChatColor.YELLOW + "Your " + sign.getLine(3) + " chest in " + world.getName() + " at (" + x + ", " + z + ") is almost out of stock" + ChatColor.RESET);
					}
					else
					{
						remove = false;
						msg.add(ChatColor.RED + "Your " + sign.getLine(3) + " chest in " + world.getName() + " at (" + x + ", " + z + ") is out of stock" + ChatColor.RESET);
					}
				}
			}

			if (remove)
				YmlManager.delShop(player.getDisplayName(), x, y, z, world.getName());
		}
		numShops = msg.size();

		if (numShops == 0)
			player.sendMessage(chatPrefix + "You have no chests out of stock!");
		else if (numShops == 1)
			player.sendMessage(chatPrefix + "You have " + ChatColor.AQUA + numShops + ChatColor.RESET + " chest out of stock:");
		else
			player.sendMessage(chatPrefix + "You have " + ChatColor.GREEN + numShops + ChatColor.RESET + " chests out of stock:");

		for (i = 0; i < numShops; i++)
			player.sendMessage(msg.get(i));
	}
}
