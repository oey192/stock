######## README for Stocks #########
####################################

Stock: A Bukkit plugin that interfaces with ChestShop to tell players if they have any ChestShop chests that are out of stock or not.

Installation:
	As with all plugins, simply copy Stock.jar to the plugins folder (located in the same directory as the server JAR file) and launch the server. If you do not have ChestShop installed, Stock will simply disable itself upon launch.

Command:
	/stock - shows the user any out of stock chests they have and their location(s)

Permissions:
	- Stock.* - gives permissions for all commands related to Stock (which is just one at the moment)
	- Stock.check - allows users to access the /stock command
		true for all players by default


ChangeLog:
    1.1.5: Add support for ChestShop 3.4 and removed an unnecessary console log. NOTE: Not backwards compatable with Chestshop 3.39!
	1.1.0: Code cleanup, excess comment removal, major bug fix: Stock no longer identifies chestshops with data values attached to material names as out of stock when they aren't. Also added chat prefix for chat messages and the ability for the plugin to disable itself if ChestShop is not loaded onto the server
	1.0.0: Initial release


WARNING! Modification of the shops.yml file may cause this plugin to produce erratic results or potentially throw unhandled exceptions! Do not modify shops.yml unless you are 300% sure that you know what you are doing AND have a very good reason to do so.
Under normal operation, this plugin will completely handle everything to do with shops.yml, so there should never be a reason to edit it. If a chestshop is not listed in shops.yml and you think it should be, contact the plugin developer at oey192@andoutay.com and give as detailled description of the problem as possible.